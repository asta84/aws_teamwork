import User from "../../../utils/dynamo/user";
import crypto from "crypto";

export default async (event) => {
  try {
    // extracting user body from request
    const body = JSON.parse(event.body);
    // checking if user already exists
    const exists = await User.query("username").eq(body.username).exec();
    if(exists !== undefined && exists.count > 0) {
        return {
            statusCode: 400,
            body: "This username is already exists"
        };
    }
    // security things
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.pbkdf2Sync(body.password, salt, 1000, 64, `sha512`).toString(`hex`);
    delete body.password;
    //creating user
    const user = await User.create({ ...body, salt, hash });
    // deleteing sensitive information
    delete user.hash;
    delete user.salt;

    return {
        statusCode: 200,
        body: JSON.stringify(user),
      };
    } catch (error) {
      /* something terrible has happened */
      console.error(error);
      return {
          statusCode: 500,
          headers: {},
          body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
          isBase64Encoded: false
      };
    }
  };