import User from "../../../utils/dynamo/user";

export default async (event) => {
  try {
    // assigning username from request to const username
    const username = event.pathParameters?.username;
    // looking for user in the table
    const user = await User.query("username").eq(username).exec();
    // checking if user exists
    if (!user || user.count < 1) {
        return {
            statusCode: 404,
            body: "User not found"
        };
    }
    // getting user from table
    const respUser = await User.get(user[0].id);
    //deleting confidential info
    delete respUser.hash;
    delete respUser.salt;

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(user[0]),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};