import User from "../../../utils/dynamo/user";
import crypto from "crypto";

export default async (event) => {
  try {
    // assigning username from request to const username
    const username = event.pathParameters?.username;
    // looking for user in the table
    const user = await User.query("username").eq(username).limit(1).exec();
    // checking if user exists
    if(user === undefined || user.count < 1) {
        return {
            statusCode: 404,
            body: "user not found"
        };
    }
    // extracting body from the request
    const body = JSON.parse(event.body);
    body.username = username;
    body.id = user[0].id;
    // security things
    if (body.password) {
        const salt = crypto.randomBytes(16).toString('hex');
        const hash = crypto.pbkdf2Sync(body.password, salt, 1000, 64, `sha512`).toString(`hex`);
        delete body.password;
        body.hash = hash;
        body.salt = salt;
    }
    // updating user
    let updatedUser = await User.update(body);
    //deleting sensetive information
    delete updatedUser.hash;
    delete updatedUser.salt;
    return {
        statusCode: 200,
        body: JSON.stringify(updatedUser),
      };
    } catch (error) {
      /* something terrible has happened */
      console.error(error);
      return {
          statusCode: 500,
          headers: {},
          body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
          isBase64Encoded: false
      };
    }
  };