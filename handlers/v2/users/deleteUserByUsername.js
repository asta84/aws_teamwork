import User from "../../../utils/dynamo/user";

export default async (event) => {
  try {
    // assigning username from request to const username
    const username = event.pathParameters?.username;
    // looking for user in the table
    const user = await User.query("username").eq(username).limit(1).exec();
    // checking if user exists
    if (!user || user.count < 1) {
        return {
            statusCode: 404,
            body: "User not found"
        };
    }
    // deleting requested user
    await User.delete(user[0].id);

    return {
        statusCode: 200,
        headers: {},
        body: JSON.stringify(`deleted user with username: ${username} (${user[0]?.firstName})`),
        isBase64Encoded: false
      };
    } catch (error) {
      /* something terrible has happened */
      console.error(error);
      return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
      };
    }
  };