import Chemical from "../../../utils/dynamo/chemical";
export default async (event) => {
  try {
	// extracting {chemicalId} from request
    const chemicalId = event.pathParameters?.chemicalId;
	// finding a chenical in the table with the same ID as provided
    const chemical = await Chemical.get(chemicalId);
	// checking if needed chemical exists in the table
    if (!chemical) {
        return {
            statusCode: 404,
            body: "Chemical not found"
        };
    }
	// deleting a chemical
    await Chemical.delete(chemical.id);

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(`deleted chemical: ${chemicalId} (${chemical.name})`),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
