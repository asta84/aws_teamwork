import Inventory from "../../../utils/dynamo/inventory";
/* Returns a map of status codes to quantities */
export default async (event) => {
	try {
		// scanning the table and adding its content to a const inventory
		const inventory= await Inventory.scan().exec();
		// checking if inventory is empty
		if (!inventory || inventory.count < 1) {
		return {
			statusCode: 404,
			body: "no inventory found"
		};
	}
	return {
		statusCode: 200,
		headers: {},
		body: JSON.stringify(inventory),
		isBase64Encoded: false
	};
	} catch (error) {
	/* something terrible has happened */
	console.error(error);
	return {
		statusCode: 500,
		headers: {},
		body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
		isBase64Encoded: false
	};
	}
  };

